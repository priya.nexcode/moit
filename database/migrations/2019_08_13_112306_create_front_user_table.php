<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrontUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email_address');
            $table->string('password');
            $table->string('VTS_number');
            $table->string('garage_name');
            $table->string('business');
            $table->text('google_address');
            $table->longText('complete_address');
            $table->string('document');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_user');
    }
}
