<html>
<head>
    <title>Front User</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
<form>
<label for="email_address">Email Address</label>
    <input type="email" name="email" id="email_address" placeholder="email Address"><br>

    <label for="password">Password</label>
    <input type="password" name="password" id="password" placeholder="password"><br>

    <label for="VTS_number">VTS Number</label>
    <input type="text" name="VTS_number" id="VTS_number" placeholder="VTS_number"><br>

    <label for="garage_name">Garage Name</label>
    <input type="text" name="garage_name" id="garage_name" placeholder="garage_name"><br>
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input type="button" value="Next" id="button">
    <label for="business_name">Business Name</label>
    <input type="text" name="business" id="business" placeholder="Business Name"><br>
    <label for="business_name">Google Address</label>
    <input type="text" id="google_address" placeholder="Google Address"><br>
    <label for="Complete_address">Complete Address</label>
    <input type="text" id="complete_address" placeholder="Complete Address"><br>
    <label for="upload_document">Upload Document</label>
    <input type="file" id="upload_document" name="upload_document"><br>
    <button type="button" onclick="btn_sub();">Submit</button>
</form>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#email_address").focusout(function () {
        var email_address = $("#email_address").val();
        console.log(email_address);
        if(email_address== ''||password=='')
        {
            console.log('email empty');
            return false;
        }
        $.ajax({
            url: '<?php echo asset('checkEmail')?>',
            type: 'POST',
            data: '_token=<?php echo csrf_token(); ?>&email_address='+email_address,
            success: function(response)
            {
                alert("success");
            }
        });
        // alert("hello");
    });

        function btn_sub() {
            var email_address = $("#email_address").val();
            var password = $("#password").val();
            var VTS_number = $("#VTS_number").val();
            var garage_name = $("#garage_name").val();
            var business = $("#business").val();
            var google_address = $("#google_address").val();
            var complete_address = $("#complete_address").val();
            var upload_document = $("#upload_document")[0].files[0];
            if(email_address== ''||password==''||VTS_number ==''||business==''||google_address==''||complete_address=='')
            {
                console.log('email');
                return false;
            }
            var fd = new FormData();
            fd.append('document', upload_document);
            fd.append('_token', '<?php echo csrf_token(); ?>');
            fd.append('email_address', email_address);
            fd.append('password', password);
            fd.append('VTS_number', VTS_number);
            fd.append('garage_name', garage_name);
            fd.append('business', business);
            fd.append('google_address', google_address);
            fd.append('complete_address', complete_address);
            $.ajax({
                url: '<?php echo asset('saveUser')?>',
                type: 'POST',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response)
                {
                    // $('#something').html(response);
                    alert("success");
                }
            });
        }
</script>
</html>
