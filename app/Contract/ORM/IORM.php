<?php
namespace App\Contract\ORM;

interface IORM
{
    public function saveUserDetails($request);
    public function checkExistingEmail($request);
    public function checkUserExisting($request);

}
