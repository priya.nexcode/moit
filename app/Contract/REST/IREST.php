<?php

namespace App\Contract\REST;

use Illuminate\Http\Request;

interface IREST
{

    public function saveUserDetails(Request $request);
    public function checkExistingEmail(Request $request);
    public function loginUser(Request $request);
}
