<?php
namespace App\Services\REST;
use App\Contract\ORM\IORM;
use App\Contract\REST\IREST;
use App\Mail\activateUser;
use App\Model\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class REST implements IREST
{
    protected $ormService;

    public function __construct(IORM $object)
    {
        $this->ormService = $object;
    }

 public function saveUserDetails(Request $request)
 {
     $inputAll = $request->all();
     Log::info("RestData".json_encode($inputAll));
     if (!isset($inputAll['email_address']) || $inputAll['email_address'] == "" || !isset($inputAll['password']) || $inputAll['password'] == "" ||
         !isset($inputAll['VTS_number']) || $inputAll['VTS_number'] == ""  || !isset( $inputAll['garage_name']) || $inputAll['garage_name'] == '')
     {
         return Response::returnFailureResponse('1', 'Failed to save user Data', '422');

//         echo 2; die;
     }
     $saveData = $this->ormService->saveUserDetails($request);
     if(!$saveData->status)
     {
         return Response::returnFailureResponse('1', 'Failed to save user Data', '422');
     }
     ////
//            $getId = base64_encode(serialize($saveData->id));
//            $actionurl = action('front_user@activeuser',$getId);
//            $link = "<a href='".$actionurl."'></a>";
     $link =  $inputAll['email_address'];
     Mail::to($inputAll['email_address'])->send(new activateUser($link));
     return Response::returnSuccessWithMessageResponse('User created successfully', $saveData->value, '201');
 }


  public function checkExistingEmail(Request $request)
  {
      $getData = $this->ormService->checkExistingEmail($request);
      Log::info("restEmial".json_encode($getData));
      if($getData->status)
      {
          Log::info("SuccesREST");
          return Response::returnSuccessWithMessageResponse('Success','',201);
      }
      Log::info("fail Rest");
          return Response::returnFailureResponse('1',"Email Address already exist",422);
  }

  public function loginUser(Request $request)
  {
      $input = $request->all();
      if(!isset($input['email_address']) || $input['email_address']=='')
      {
          return Response::returnFailureResponse(1,"Email is required",422);
      }
      if(empty($input['password']) || $input['password']=='')
      {
          return Response::returnFailureResponse(1,"password is Required",422);
      }
      $result = $this->ormService->checkUserExisting($request);
      if($result->status)
      {

          if($result->value['password'] != $this->hashPassword($request['password']))
              {
                  return Response::returnFailureResponse(1,"UserName/Password Incorrect",422);
              }
          else{
                return Response::returnSuccessWithMessageResponse("SuccessFully log User",'',201);
              Log::info("success");
          }
      }
      return Response::returnFailureResponse(1,"User Not Found",422);
  }






}
