<?php
namespace App\Services\ORM;
use App\Contract\ORM\IORM;
use App\Mail\activateUser;
use App\Model\FrontUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Model\Response;

class ORM implements IORM
{
public function saveUserDetails($request)
{
    $inputAll = $request->all();
  Log::info("getAllOrmData".json_encode($inputAll));
    DB::beginTransaction();
    try {
        $saveData = new FrontUser();
        if (isset($inputAll['email_address']) && !empty($inputAll['email_address']) && $inputAll['email_address'] != null) {
            $saveData->email_address = $inputAll['email_address'];
        }
        if (isset($inputAll['password']) && !empty($inputAll['password']) && $inputAll['password'] != null) {
            $saveData->password = md5($inputAll['password']);
        }
        if (isset($inputAll['VTS_number']) && !empty($inputAll['VTS_number']) && $inputAll['VTS_number'] != null) {
            $saveData->VTS_number = $inputAll['VTS_number'];
        }
        if (isset($inputAll['garage_name']) && !empty($inputAll['garage_name']) && $inputAll['garage_name'] != null) {
            $saveData->garage_name = $inputAll['garage_name'];
        }
        if (isset($inputAll['business']) && !empty($inputAll['business']) && $inputAll['business'] != null) {
            $saveData->business = $inputAll['business'];
        }
        if (isset($inputAll['google_address']) && !empty($inputAll['google_address']) && $inputAll['google_address'] != null) {
            $saveData->google_address = $inputAll['google_address'];
        }
        if (isset($inputAll['complete_address']) && !empty($inputAll['complete_address']) && $inputAll['complete_address'] != null) {
            $saveData->complete_address = $inputAll['complete_address'];
        }
        if (isset($inputAll['document']) && !empty($inputAll['document']) && $inputAll['document'] != null) {
            if($request->file('document')){
                $docimg = $request->file('document');
                $filename = 'user-'.time().'.'.$docimg->getClientOriginalExtension();
                $filepath = public_path('UserImage/');
                $docimg->move($filepath,$filename);
//                    $saveData->document = asset(($filepath,$filename);
                $saveData->document = 'public/UserImage/'.$filename;
            }

        }
        DB::commit();
//            Image::make('public/UserImage/'.$filename)->resize(50,50);

        if ($saveData->save()) {
////
//            $getId = base64_encode(serialize($saveData->id));
//            $actionurl = action('front_user@activeuser',$getId);
//            $link = "<a href='".$actionurl."'></a>";
//            \Mail::to($inputAll['email_address'])->send(new activateUser($link));
            Log::info("savedata");
            return Response::returnSuccessWithMessageResponse("Success", '', 200);

//            echo 1; die;
        }
    } catch (\Exception $exception) {
        DB::rollBack();
        return Response::returnFailureResponse(1, ' Failed to save data', 404);

//        echo 2;
    }

}


public function checkExistingEmail($request)
{
    $checkEmail = FrontUser::where('email_address',$request['email_address'])->first();
    Log::info("ormEmail".json_encode($checkEmail));
    if(empty($checkEmail) || $checkEmail == null)
    {
        Log::info("successs");
        return Response::returnSuccessWithMessageResponse("Success", '', 200);
    }
    Log::info("email exist");
    return Response::returnFailureResponse(1, ' Please enter another Email address ,Email Address already exist', 404);
}

  public function checkUserExisting($request)
  {
      $result = FrontUser::where('email_address',$request['email_address'])->first();
      if($result)
      {
          return Response::returnSuccessResponse('$result',200);
      }
        return Response::returnFailureResponse(1,"Email Does Not Matched",404);
  }



}
