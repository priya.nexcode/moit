<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class activateUser extends Mailable
{
    use Queueable, SerializesModels;

    protected $link;
    public function __construct($link)
    {
        $this->link = $link;
    }

    public function build()
    {
        return $this->view('activate',array("link"=>$this->link))->subject("user Activation");

    }
}
