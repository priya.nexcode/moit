<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ResponseModel extends Model
{

    protected $visible = ['error','status', 'success', 'data', 'message', "errorCode"];

    protected $fillable = [
        'data', 'status', 'message',
    ];
}
