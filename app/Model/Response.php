<?php
namespace App\Model;

class Response{
    public $errorCode;
    public $errorMessage;
    public $status;
    public $httpCode;
    public $value;
    public static function returnSuccessResponse($value, $httpCode)
    {
        $res = new Response();
        $res->value = $value;
        $res->httpCode = $httpCode;
        $res->status = true;
        return $res;
    }
    public static function returnFailureResponse($errorCode, $errorMessage, $httpCode)
    {
        $response = new Response();
        $response->errorCode = $errorCode;
        $response->errorMessage = $errorMessage;
        $response->status = false;
        $response->httpCode = $httpCode;
        return $response;
    }
    public static function returnSuccessWithMessageResponse($message, $data, $httpCode)
    {
        $res = new Response();
        $res->value = $data;
        $res->message = $message;
        $res->httpCode = $httpCode;
        $res->status = true;
        return $res;

    }

    public static function download($filetopath, $zipFileName, $headers)
    {
    }
}
