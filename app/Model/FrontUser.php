<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;


class FrontUser extends Model
{
    use HasApiTokens;
    protected $table = "front_user";

}
