<?php
namespace App\Providers\REST;
use App\Contract\REST\IREST;
use App\Services\REST\REST;
use Illuminate\Support\ServiceProvider;

class RESTService extends ServiceProvider
{

    public function boot()
    {
        //
    }
    public function register()
    {

        $this->app->bind(IREST::class, REST::class);

    }

}
