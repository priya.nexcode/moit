<?php

namespace App\Providers\ORM;

use App\Contract\ORM\IORM;
use App\Services\ORM\ORM;
use Illuminate\Support\ServiceProvider;

class ORMService extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IORM::class, ORM::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
