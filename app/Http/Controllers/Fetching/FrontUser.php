<?php

namespace App\Http\Controllers\Fetching;
use App\Contract\REST\IREST;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Model\Response;
use App\Mail\activateUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class FrontUser extends BaseController
{
    protected $restServices;

    public function __construct(IREST $object)
    {
        $this->restServices = $object;
    }

    private function hashPassword($password)
    {
        return md5($password);
    }

    public function saveUser(Request $request)
    {
        $inputAll = $request->all();
//        Log::info("ControllerData".json_encode($inputAll));

        $resultData = $this->restServices->saveUserDetails($request);
        if($resultData->status==false)
        {
            return $this->sendError($resultData->errorMessage, $resultData->errorCode, $resultData->httpCode);

        }
        return $this->sendSuccess($resultData->message, $resultData->value, 200);



//      echo"<pre>"; print_r($inputAll); die;
//        if (!isset($inputAll['email_address']) || $inputAll['email_address'] == "" || !isset($inputAll['password']) || $inputAll['password'] == "" ||
//           !isset($inputAll['VTS_number']) || $inputAll['VTS_number'] == ""  || !isset( $inputAll['garage_name']) || $inputAll['garage_name'] == '')
//        {
//            echo 2; die;
//        }
//        echo"<pre>"; print_r($inputAll); die;
//        DB::beginTransaction();
//        try {
//            $saveData = new FrontUser();
//            if (isset($inputAll['email_address']) && !empty($inputAll['email_address']) && $inputAll['email_address'] != null) {
//                $saveData->email_address = $inputAll['email_address'];
//            }
//            if (isset($inputAll['password']) && !empty($inputAll['password']) && $inputAll['password'] != null) {
//                $saveData->password = md5($inputAll['password']);
//            }
//            if (isset($inputAll['VTS_number']) && !empty($inputAll['VTS_number']) && $inputAll['VTS_number'] != null) {
//                $saveData->VTS_number = $inputAll['VTS_number'];
//            }
//            if (isset($inputAll['garage_name']) && !empty($inputAll['garage_name']) && $inputAll['garage_name'] != null) {
//                $saveData->garage_name = $inputAll['garage_name'];
//            }
//            if (isset($inputAll['business']) && !empty($inputAll['business']) && $inputAll['business'] != null) {
//                $saveData->business = $inputAll['business'];
//            }
//            if (isset($inputAll['google_address']) && !empty($inputAll['google_address']) && $inputAll['google_address'] != null) {
//                $saveData->google_address = $inputAll['google_address'];
//            }
//            if (isset($inputAll['complete_address']) && !empty($inputAll['complete_address']) && $inputAll['complete_address'] != null) {
//                $saveData->complete_address = $inputAll['complete_address'];
//            }
//            if (isset($inputAll['document']) && !empty($inputAll['document']) && $inputAll['document'] != null) {
//                if($request->file('document')){
//                    $docimg = $request->file('document');
//                    $filename = 'user-'.time().'.'.$docimg->getClientOriginalExtension();
//                    $filepath = public_path('UserImage/');
//                    $docimg->move($filepath,$filename);
////                    $saveData->document = asset(($filepath,$filename);
//                    $saveData->document = 'public/UserImage/'.$filename;
//                }
//
//            }
//            DB::commit();
////            Image::make('public/UserImage/'.$filename)->resize(50,50);
//
//            if ($saveData->save()) {
//
//                $getId = base64_encode(serialize($saveData->id));
//                $actionurl = action('front_user@activeuser',$getId);
//                $link = "<a href='".$actionurl."'></a>";
//                Mail::to($inputAll['email_address'])->send(new activateUser($link));
//                echo 1; die;
//            }
//        } catch (\Exception $exception) {
//            DB::rollBack();
//            echo 2;
//        }

    }


    public function checkEmail(Request $request)
    {
        $getData = $request->all();
        Log::info("getemailData".json_encode($getData));
        if(isset($getData) && !empty($getData));
        {


            $resultData = $this->restServices->checkExistingEmail($request);
            Log::info("hid".json_encode($resultData));
            if($resultData->status==false)
            {
                Log::info("fail controller");
                return $this->sendError($resultData->errorMessage, $resultData->errorCode, $resultData->httpCode);
            }
            Log::info("sssssuccess Controller");
            return $this->sendSuccess($resultData->message, $resultData->value, 200);






//
//            $checkEmail = FrontUser::where("email_address",$getData['email_address'])->get();
//            if($checkEmail)
//            {
//                return Response::returnSuccessWithMessageResponse("Matched Email Address",$checkEmail,200);
//            }
//            return Response::returnFailureResponse(1,"Email not Matched",404);

        }

    }



    public function loginUser(Request $request)
    {

        $result  =  $this->restServices->loginUser($request);
        if($result)
        {
            return $this->sendSuccess("Successfully login User", $result,200);
        }
        return $this->sendError($result->errorMessage, $result->errorCode, $result->httpCode);
    }


}
