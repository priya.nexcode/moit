<?php

namespace App\Http\Controllers;

use App\Model\ResponseModel;

class BaseController extends Controller
{
    //Class constructor, called automatically when the class's object is initialized.
    public function __construct()
    {

    }

    public function sendError($message, $errorCode, $httpCode)
    {
        $resp = new ResponseModel();
        $resp->status = false;
        $resp->error = $message;
        $resp->errorCode = $errorCode;
        return response($resp, $httpCode);
    }

    public function sendSuccess($message, $data, $httpCode)
    {
        $resp = new ResponseModel();
        $resp->status = true;
        $resp->message = $message;
        $resp->data = $data;
        return response($resp, $httpCode);

    }
}
